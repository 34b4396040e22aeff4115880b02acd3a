### Keybase proof

I hereby claim:

  * I am grantmeadors on github.
  * I am grantmeadors (https://keybase.io/grantmeadors) on keybase.
  * I have a public key whose fingerprint is 4168 DE44 D0C7 B121 13CE  4CD9 9DA8 1C67 9290 DE28

To claim this, I am signing this object:

```json
{
    "body": {
        "key": {
            "eldest_kid": "0101e0d14e14d3298c645225fbc0e3a48a43fa258b9df355cfe35cd4aacb421d90a10a",
            "fingerprint": "4168de44d0c7b12113ce4cd99da81c679290de28",
            "host": "keybase.io",
            "key_id": "9da81c679290de28",
            "kid": "0101e0d14e14d3298c645225fbc0e3a48a43fa258b9df355cfe35cd4aacb421d90a10a",
            "uid": "c1ebc99a2c760126bf298ac50df9eb19",
            "username": "grantmeadors"
        },
        "service": {
            "name": "github",
            "username": "grantmeadors"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "ctime": 1487010539,
    "expire_in": 157680000,
    "prev": "4f9dfda0e71581ef27722ac93351d19ea1618565d6e013a1cfd25c2bc02f4263",
    "seqno": 4,
    "tag": "signature"
}
```

with the key [4168 DE44 D0C7 B121 13CE  4CD9 9DA8 1C67 9290 DE28](https://keybase.io/grantmeadors), yielding the signature:

```
-----BEGIN PGP MESSAGE-----
Comment: GPGTools - https://gpgtools.org

owGtUltIFFEYXrf7iiQWYmAagw9SKnPOzJmdWakHnwLDDAsMwe3MOWfWyZrdZsdt
xQuRlCgIKkoR3nqIyh6Ukm4kQal0s8AH8SUlasOoyB6CEi+dkXyLnjovh/Of7/v4
vo+/PWWDx5fUeyMz1Nk+m5v08pPuKb+6uFQn6GFaKwTqhGq2drFTlEWdYLVJhYAg
AhEwkQKZAZlKUFOJIiMIkaETkUlYVrEsGRgiVdeoISFEDCYhQmWMiS5DQDURAxEL
eYJhWiFmR2zTcrisDBSVMlmmIvHrAAIgESYTqmkUq4Aofg1qImVQ5cSqcNRlcHM6
jrICM8xn/BFcs/cX/H/2XbMmRwDTiaZhSPyKCKCiG1wSEyRSQ2M60FxglNkWPs04
OmRjyznNMA3bUaEhT+A/MZMwt9x1hOlU1ej/Zjm1EXd8lunBPwJB3bQo75HzYsyO
mmFLCACOJI7pKgBZ9fPcSNLyBBaPmDYLmi4C+RVV5CdPiNgs5rZv8NAUl4vMD5AK
mAH9fggx0SQJAQo0hoECVKQgqjARSBgQg0JEIO8OGjJUJMHNdMYKCwGZ+8Qhrhk1
QxZ2amwmNPhakjI3epJ8ns2bvO6GeXzbUtfXTj+41XNv+Uhrn/Po4uro7aKyfecS
1Wk/Bqcf92BrKTIIkz97nk2W109+yRBfjDjfvxaXvm0cKk77mR3rRE1o57HL3b8e
jqy86Pq851t5znz7p4neD8nPgxsOjXtz3izMDAwseL2vSf6p/qm+ejxtLcVy3j+4
2dIdREP9Kdm7sy6UpGuzrU5+XUKpSGzJgD+zx3KrSnsrybXDw68Lr7wLms0rFXZG
4GnW8bFudL9pZjJ9x3ThyW1VHQkQn7vU/Cr3TteBdLOx8mPx9r7Nz9no/O7+vp69
gayO8fhCWVxc/WVNT6S9WtyfWtLWljG1fKJo1/XhgrvyrUR36tG5Jzfmc8//2P4b
=R0TW
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/grantmeadors

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id grantmeadors
```
